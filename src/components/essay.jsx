import React, { Component } from 'react';
import Tabs from "./tabs";
import "../stylesheets/essay.css";

import YouTube from 'react-youtube';

class Background extends Component {

  render() {

    const opts = {
      height: '390',
      width: '640',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };

    return (
      <div>
        <YouTube
          className="VideoAlign"
          videoId='kvthRNdu14I'
          opts={opts}
          onReady={this._onReady}
        />
        <h3>Comparison of Sign to Spoken:</h3>
        <p>They talk. We don’t.</p>
        <p>While there obviously is a difference between spoken and signed languages in the manner of using sound or not, the lack of a sense was usually seen as a disadvantage in the old world. However, what we’ve come to realize through our experiences with the constant need for discretion, due to zombie hordes lurking, is that our new way of communication has its benefits too. It’s become incredibly easy to communicate basicly. </p>
        <p>We’ve removed complex sentence structures in order to focus on what’s vital for our new civilization. In a similar manner, according to brittanica.com “hieroglyphs were from the very beginning phonetic symbols” (Dorman & Brunner, 2017, p. 1). While hieroglyphs were focusing on taking a bunch of phonetic symbols and combining them to form more complex meanings, our system focuses on taking signs for nouns, verbs, emotions and more and combining them to form more complex ideas.</p>
        <p>Right now, it’s imperative for The Collective to start from scratch and develop language through our most rudimentary needs. Signs for things such as water and zombies are much more important than other nouns people might be introduced to when first learning a language such as bathroom or dad. Emotions are also much needed. Understanding feelings is one of our most primitive functions and growing that function through language can help improve our odds of survival.</p>

        <h3>Record-Keeping System:</h3>
        <p>With the fall of the English language (as we type in English), we were forced to devise a new language and way of recording it to pass along to future generations (assuming the zombies do not get to us first). </p>
        <p>We drew inspiration from the Egyptians when creating our record-keeping system. Egyptian hieroglyphs are pictures of objects used to represent words, sounds, or meanings. Our language consists of signs with symbolic meaning. So, we decided to record these as pictures to depict the sign motions. Pictures help with visualizing the motion.</p>
        <p>Similar to the Egyptians, we will etch these on paper and stone. One such place Egyptians did this was on pillars. One relief found by the University of Illinois, Urbana- Champaign had hieroglyphics on it and “[was] slightly rounded indicating that it was part of a very large column” (“Egyptian Relief”, 1968, p. 1). Paper records can move with us should we need to flee in the event of a zombie attack. Stone etchings offer more permanent solutions.</p>

        <h3>Limitations of Language and Record-Keeping System:</h3>
        <p>Although we see these as advantages for our current state, maintaining a more primitive language is very limiting in that it becomes difficult to develop ideas and make complex plans beyond survival.</p>
        <p>There are three other limitations my team and I have come up with. These pertain more so to the physical aspects of the language rather than the concepts. The first is restriction. Our language requires hand motions and body language and there will more likely than not be times when you are in a situation of restricted motion. Furthermore, we see a obvious limitation that is similar to one of any language: Impairments. Just as the deaf are limited in spoken language. People who are blind or motion restricting impairments won’t be able to use this form of communication. I mentioned the fact that the blind cannot utilize our sign, but even more pressing is the issue that if you aren’t directly looking at the person trying to communicate, you won’t know anything. On top of all that, our system makes it hard to do advanced math as a person only has 10 fingers. A more advanced technique like cuneiform uses “modelling clay and a stylus” (Robson, 1998, p. 2). These tools allow a more advanced system of numbers than signs can. There are much fewer physical constraints with clay and a stylus than there are with 10 fingers. These are stressful times and we need to adapt. We are limited by our communication, but we will have to make due and be intentional with each action we take.</p>

        <h3>Influence of Language and Record-Keeping System on Population:</h3>
        <p>Each language and record keeping system come with a sort of literacy. In this case, our system forces the population to be literate in the symbols signed and written down on paper and stone. Those who have learned these symbols will have a distinct advantage over those who don’t. For example, those that know signs in a market could trade effectively, but if a person does not know a sign, they could be swindled into taking a bad deal.</p>
        <p>Furthermore, our language and record keeping system has a positive effect on our population based on the environment. Considering zombies love noise, being able to communicate without making any, is a major plus. On top of this, if there is no sound being produced, sounds of enemies, prey, or zombies approaching will all be amplified which makes staying alive easier.</p>
        <p>Next, this form of communication and record keeping would likely lead to different “accents” and therefore potentially deadly confusion. This would be a sort of Tower of Babel situation. The Tower of Babel’s “purpose was to account for… the diversity of language” (“The Tower of Babel”, 1892, p. 268). In this biblical story, God made the powerful Babylonians speak different languages in order to make them less powerful. If for whatever reason our population breaks up and is living apart from one another for a long time, they are likely to develop signs that are different from one another. Since this system is visual, signs are largely based on what people can see. For example, a group living in a warm environment and one living in a cold environment could have the same symbol, but one could mean snow and the other could mean rain. Based on food sources, nearby enemies, and other location based factors, these signs could get mixed up with other subpopulations and cause things to go haywire. Therefore, our situation would be similar to the Tower of Babel because different “accents” would cause the population to be less powerful.</p>
        <p>Finally, our population will most likely adapt to be one that lives and functions very close and as a group. This is neither inherently good or bad. Since one must be within eyesight to communicate using this system, it only makes sense that the population stays close together. While this helps our population survive some scenarios, such as a lone zombie, others, like a plague, would be far more dangerous in a tight group.</p>

        <h3>References</h3>
        <p>Dorman, P. F., & Brunner, H. (2017, October 17). Hieroglyphic Writing. In Britannica.com. Retrieved October 2, 2018, from https://www.britannica.com/topic/hieroglyphic-writing</p>
        <p>Egyptian Relief (University of Illinois Krannert Art Museum, Urbana-Champaign). (1968). The Burlington Magazine, 110(788), 630-632. Retrieved from http://www.jstor.org/stable/875821</p>
        <p>Robson, E. (1998). Counting in Cuneiform. Mathematics in School, 27(4), 2-9. Retrieved from http://www.jstor.org/stable/30211866</p>
        <p>The Tower of Babel. (1892). The Old and New Testament Student,15(5/6), 268-268. Retrieved from http://www.jstor.org/stable/3157778</p>
      </div>
    );
  }
  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}

export default Background;
