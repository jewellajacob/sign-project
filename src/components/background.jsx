import React, { Component } from 'react';
import Tabs from "./tabs";
import "../stylesheets/background.css";

class Background extends Component {

  render() {
    return (
      <div>
        <div className="TitleContainer">
          <h2 className="TitleHeadText">How to Talk Around Zombies for Dummies</h2>
        </div>
        <Tabs/>
      </div>
    );
  }
}

export default Background;
