import React, { Component } from 'react';
import Content from './content.jsx';
import Essay from './essay.jsx';
import "../stylesheets/container.css";

class Container extends Component {
  state = {
    vocabOne: [
      {
        id: 1,
        videoId: 'nXCdnemRYVk',
        title: '',
        description: ''
      },
      {
        id: 2,
        videoId: null,
        title: 'Sibling (noun):',
        description: 'offspring having one or both parents in common'
      },
      {
        id: 3,
        videoId: null,
        title: 'Fear (noun):',
        description: 'emotion caused by belief that something is dangerous'
      },
      {
        id: 4,
        videoId: null,
        title: 'Parent (noun):',
        description: 'a father or mother'
      },
      {
        id: 5,
        videoId: null,
        title: 'Lose (verb):',
        description: 'be deprived of'
      },
      {
        id: 6,
        videoId: null,
        title: 'Talk (verb):',
        description: 'converse by spoken word'
      },
      {
        id: 7,
        videoId: null,
        title: 'Think (verb):',
        description: 'have an idea about something'
      },
      {
        id: 8,
        videoId: null,
        title: 'Become (verb):',
        description: 'begin to be'
      },
      {
        id: 9,
        videoId: null,
        title: 'Child (noun):',
        description: 'a young human'
      }
    ],
    vocabTwo: [
      {
        id: 1,
        videoId: 'kMpMGzTPYho',
        title: '',
        description: ''
      },
      {
        id: 2,
        videoId: null,
        title: 'Forever (adverb):',
        description: 'for all future time'
      },
      {
        id: 3,
        videoId: null,
        title: 'Find (verb):',
        description: 'discover something to be present'
      },
      {
        id: 4,
        videoId: null,
        title: 'Dark (adjective):',
        description: 'with little or no light'
      },
      {
        id: 5,
        videoId: null,
        title: 'Disappointed (adjective):',
        description: 'displeased as something fails to fulfill expectations'
      },
      {
        id: 6,
        videoId: null,
        title: 'Yay (informal):',
        description: 'expressing triumph'
      },
      {
        id: 7,
        videoId: null,
        title: 'Dead (adjective):',
        description: 'no longer alive'
      }
    ],
    vocabThree: [
      {
        id: 1,
        videoId: 'Ee9o1QVOtUY',
        title: '',
        description: ''
      },
      {
        id: 2,
        videoId: null,
        title: 'Zombie (noun):',
        description: 'a revived corpse'
      },
      {
        id: 3,
        videoId: null,
        title: 'Water (noun):',
        description: 'this a liquid you gonna need to live'
      },
      {
        id: 4,
        videoId: null,
        title: 'Destroy (verb):',
        description: 'put an end to its existence'
      },
      {
        id: 5,
        videoId: null,
        title: 'Gun (noun):',
        description: 'pow pow the end, kill the zombies'
      },
      {
        id: 6,
        videoId: null,
        title: 'Protect (verb):',
        description: 'shield those you love from danger'
      },
      {
        id: 7,
        videoId: null,
        title: 'Brave (adjective):',
        description: 'you have all the courage, so kill them zombies'
      }
    ],
    vocabFour: [
      {
        id: 1,
        videoId: 'j4SMg_Rlfi0',
        title: null,
        description: null
      },
      {
        id: 2,
        videoId: null,
        title: 'Cat (noun)',
        description: 'meow, these are furry friends. Eat them to survive'
      },
      {
        id: 3,
        videoId: null,
        title: 'I (noun)',
        description: 'this is who you are, you need this to live'
      },
      {
        id: 4,
        videoId: null,
        title: 'Win (verb)',
        description: 'when you come out on top of everyone else'
      },
      {
        id: 5,
        videoId: null,
        title: 'Truth (noun)',
        description: 'the state of being true'
      },
      {
        id: 6,
        videoId: null,
        title: 'Deaf (adjective)',
        description: 'the state of not being able to hear'
      },
      {
        id: 7,
        videoId: null,
        title: 'Friend (noun)',
        description: 'hopefully someone you can rely on, a companion'
      },
      {
        id: 8,
        videoId: null,
        title: 'Like (noun)',
        description: 'used to signify some affection towards something'
      },
      {
        id: 9,
        videoId: null,
        title: 'Love (noun)',
        description: 'used to signify a whole lot of affection'
      }
    ]
  };

  render() {

    return (
      <div className="BodyContainer">
        <div
          className={this.props.id === 1 ? '': 'DisplayNone'}
        >
          <Essay></Essay>
        </div>
        <div
          className={this.props.id === 2 ? '': 'DisplayNone'}
        >
          { this.state.vocabOne.map(content =>(
            <Content
              key={content.id}
              videoId={content.videoId}
              title={content.title}
              description={content.description}
            />
          ))}
        </div>
        <div
          className={this.props.id === 3 ? '': 'DisplayNone'}
        >
          { this.state.vocabTwo.map(content =>(
            <Content
              key={content.id}
              videoId={content.videoId}
              title={content.title}
              description={content.description}
            />
          ))}
        </div>
        <div
          className={this.props.id === 4 ? '': 'DisplayNone'}
        >
          { this.state.vocabThree.map(content =>(
            <Content
              key={content.id}
              videoId={content.videoId}
              title={content.title}
              description={content.description}
            />
          ))}
        </div>
        <div
          className={this.props.id === 5 ? '': 'DisplayNone'}
        >
          { this.state.vocabFour.map(content =>(
            <Content
              key={content.id}
              videoId={content.videoId}
              title={content.title}
              description={content.description}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Container;
