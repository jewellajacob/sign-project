import React, { Component} from 'react';
// import AboutMe from "./aboutme";
// import Resume from "./resume";
// import Projects from "./projects";
import Container from './container.jsx';
import '../stylesheets/tabs.css';

class Tabs extends Component {
  constructor(props) {
        super(props);
        this.toggleClass= this.toggleClass.bind(this);
        this.state = {
            firstActive: true,
            secondTab: false,
            thirdTab: false,
            fourthTab: false,
            fifthTab: false,
            tabNumber: 1
        };
    }

  toggleClass(tab) {
    let firstTab = null;
    let secondTab = null;
    let thirdTab = null;
    let fourthTab = null;
    let fifthTab = null;
    let tabNumber = tab;
    if (tab === 1) {
      firstTab = true;
      secondTab = false;
      thirdTab = false;
      fourthTab = false;
      fifthTab = false;
    }
    else if (tab === 2) {
      firstTab = false;
      secondTab = true;
      thirdTab = false;
      fourthTab = false;
      fifthTab = false;
    }
    else if (tab === 3) {
      firstTab = false;
      secondTab = false;
      thirdTab = true;
      fourthTab = false;
      fifthTab = false;
    }
    else if (tab === 4){
      firstTab = false;
      secondTab = false;
      thirdTab = false;
      fourthTab = true;
      fifthTab = false;
    }
    else if (tab === 5){
      firstTab = false;
      secondTab = false;
      thirdTab = false;
      fourthTab = false;
      fifthTab = true;
    }
    this.setState({
      firstActive: firstTab,
      secondActive: secondTab,
      thirdActive: thirdTab,
      fourthActive: fourthTab,
      fifthActive: fifthTab,
      tabNumber: tabNumber
    });
  };

  render() {
    return(
      <div className="MainContainer">
        <div className="TabContainer">
          <div
            onClick={() => this.toggleClass(1)}
            className={this.state.firstActive ? 'ActiveTab': 'Tab'}
          >
            Welcome!
          </div>
          <div
            onClick={() => this.toggleClass(2)}
            className={this.state.secondActive ? 'ActiveTab': 'Tab'}
          >
            Vocab One
          </div>
          <div
            onClick={() => this.toggleClass(3)}
            className={this.state.thirdActive ? 'ActiveTab': 'Tab'}
          >
            Vocab Two
          </div>
          <div
            onClick={() => this.toggleClass(4)}
            className={this.state.fourthActive ? 'ActiveTab': 'Tab'}
          >
            Vocab Three
          </div>
          <div
            onClick={() => this.toggleClass(5)}
            className={this.state.fifthActive ? 'ActiveTab': 'Tab'}
          >
            Vocab Four
          </div>
        </div>
        <div>
          <Container
            id={this.state.tabNumber}
          ></Container>
        </div>
      </div>
    );
  }


}

export default Tabs;
