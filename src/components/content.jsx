import React, { Component } from 'react';
import "../stylesheets/container.css";
import "../stylesheets/content.css";

import YouTube from 'react-youtube';

class Content extends Component {
  render() {

    const opts = {
      height: '390',
      width: '640',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };
    let video;
    if (this.props.videoId) {
      video=<YouTube
        className="VideoAlign"
        videoId={ this.props.videoId }
        opts={opts}
        onReady={this._onReady}
      />
    } else {
      video=<p></p>
    }

    return (
      <div>
        {video}
        <h2 className='WordText'>
          { this.props.title }
        </h2>
        <p className='DescText'>
          { this.props.description }
        </p>
      </div>
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }

}

export default Content;
